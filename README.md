# Update-Twitter-Banner.py

updateTwitterBanner.py utilizes Python3 and the Python Tweepy library to communicate with Twitters API and update the specified profiles Banner and Description.

## Installation

Get the latest version of Python here - https://www.python.org/getit/

Link to Tweepy repository - https://github.com/tweepy/tweepy

```bash
pip install tweepy
```

## Usage
Note: Specified Paths in the code MAY need to be edited.

1. Insert any desired profile banner pictures (ONLY TESTED WITH .PNG)
2. Edit the updateTwitterBannerSecerets.py and input YOUR API-Keys here.
3. run the updateTwitterBannerSecerets.py to ensure PATH/API-Tokens are correct.
4. [OPTIONAL] Create a cron-job to automatically run script multiple times throughout the day.

[Cron]

Open up the crontab editor 
```bash
$ crontab -e
```
Append the following entry to the cron file. Edit /path/to with values that reflect where the program is stored on your machine.

```bash
0 * * * * python3 /path/to/updateTwitterBanner.py >> /path/to/bannerCronLog.txt

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
