import os
import random
import tweepy
import updateTwitterBannerSecrets as secrets

#CONST
#Used to hold value of screen shot path.
file = ''

#Choose a random photo in album
def randomPicture():
    global file
    directory = 'ScreenShots/'
    counter = 0
    randomChoice = random.randint(1, len(os.listdir(directory)))
    for filename in os.listdir(directory):
        if counter == randomChoice:
            f = os.path.join(directory, filename)
            file = f
            break
        elif counter != randomChoice:
            counter += 1
        else:
            print("randomPicture() if/elif has an issue, recalling randomPicture()")
            randomPicture()

#Main function that authenticates with twitter API and updates profile banner/description
def main():
    global file
    #Specify API keys and Access Token to authenticates with Twitter API
    auth = tweepy.OAuth1UserHandler(secrets.api_key, secrets.api_secret, secrets.access_token, secrets.access_token_secret)
    #Authenticates with Twitter API useing the credentials in auth.
    api = tweepy.API(auth)
    #Picture for New Twitter Banner
    randomPicture()
    #removes path to file
    removePath = file.replace("ScreenShots/","")
    #removes .png from file name to be used in profile description
    colors = removePath.replace(".png", "")
    #Adds current banner colors to profile description TODO: get the current description first before updating it.
    #NOTE:Manually input desired twitter banner here OR comment out if this is undesired.
    description = "| Current Banner Colors: %s" %(colors)
    #Updates the twitter banner to the .png specified in the file variable
    #NOTE: The height/width here specifies the L/W of screenshot. (Make these == Current Monitor Resolution i.e. 1920x1080.) 
    updateBanner = api.update_profile_banner(file, height='800px', width='1300px')
    #Updates the twitter description to formatted description with 2 RGB colors
    updateDescription = api.update_profile(description=description)
    #Print the respone from twitter API, should be None if successful
    print("Updating Twitter Banner to %s... Twitter Respone: %s" %(colors, updateBanner))
    #print(file, colors, description)

#Start of Program
if __name__ == "__main__":
    main()
